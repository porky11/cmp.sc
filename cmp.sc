fn bin* (op f)
    "Creates a variadic function from a binary function"
    fn (a b ...)
        if (none? b)
            a
        elseif (none? ...)
            f a b
        else
            op
                f a b
                (bin* op f) b ...

let == != <= < > >= =
    bin* (do &) ==
    bin* (do |) !=
    bin* (do &) <=
    bin* (do &) <
    bin* (do &) >
    bin* (do &) >=

if main-module?
    print "Testing comparison operators with multiple values"
    fn test-print (name f ...)
        print (if (none? name) "arguments: ") (else ("test `" .. name .. "`:`"))
            f 1 2 3 4
            f 1 1 2
            f 1 2 1
            f 4 4 4 4
        if (not (none? ...))
            test-print ...

    test-print none list "==" (do ==) "!=" (do !=) "<=" (do <=) "<" (do <) ">" (do >) ">=" (do >=)

locals;
